<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset=utf-8>
	<title>¡Seguimiento en directo!</title>

	<script src="http://openlayers.org/api/OpenLayers.js"></script>
	<script type="text/javascript" src="js/prototype.js"></script>
	<script type="text/javascript" src="js/common.js"></script>
	<script type="text/javascript" src="js/custom.js"></script>
	<script>
		var map, zoom=15, team1Marker, team1Popup, team2Marker, team2Popup, team3Marker, team3Popup,
			team4Marker, team4Popup, team5Marker, team5Popup, team6Marker, team6Popup,
			team7Marker, team7Popup, goalMarker;
		
		function init()
		{
			screen = new Ajax.PeriodicalUpdater('', 'jsonencoder.php', { method: 'get', frequency: 1.0, onSuccess: function(t) {
				var data = t.responseText.evalJSON();
				team1Data = data[0].toString().split("_");
				
				if (typeof(map) === "undefined") {
					initalizeMap();
				}
				
				updateMarkers();
			}});

			// if ( isset($_GET["latlon"])) {
			// 	team1Data = $_GET["latlon"].toString().split("_");

			// 	if (typeof(map) === "undefined") {
			// 		initalizeMap();
			// 	}
				
			// 	updateMarkers();
			// }
		}
		
		function initalizeMap() {
			map = new OpenLayers.Map ("mapcanvas", {
	            controls:[
	                new OpenLayers.Control.Navigation(),
	                new OpenLayers.Control.PanZoomBar(),
	                new OpenLayers.Control.LayerSwitcher(),
	                new OpenLayers.Control.Attribution()],
	            maxExtent: new OpenLayers.Bounds(-20037508.34,-20037508.34,20037508.34,20037508.34),
	                maxResolution: 156543.0399,
	            numZoomLevels: 18,
	            units: 'm',
	            projection: new OpenLayers.Projection("EPSG:900913"),
	            displayProjection: new OpenLayers.Projection("EPSG:4326")
	        } );
				
			layerMapnik = new OpenLayers.Layer.OSM();
	        map.addLayer(layerMapnik);
	        layerMarkers = new OpenLayers.Layer.Markers("Markers");
	        map.addLayer(layerMarkers);
			
	        var initLonLat = new OpenLayers.LonLat(-1.988316, 43.320236).transform(new OpenLayers.Projection("EPSG:4326"), map.getProjectionObject());
	        map.setCenter (initLonLat, zoom);
			
			var team1LonLat = new OpenLayers.LonLat(team1Data[1], team1Data[0]).transform(new OpenLayers.Projection("EPSG:4326"), map.getProjectionObject());
			var goalLonLat = new OpenLayers.LonLat(-1.985959, 43.320920).transform(new OpenLayers.Projection("EPSG:4326"), map.getProjectionObject());
			
	        var size = new OpenLayers.Size(21,25);
			var size2 = new OpenLayers.Size(31,35);
	        var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);
			
			var goal = new OpenLayers.Icon('http://www.saulcintero.com/gpstracker/img/chequered_flag.png',size2,offset);
	        var icon = new OpenLayers.Icon('http://www.saulcintero.com/gpstracker/img/marker1.png',size,offset);
				
	        team1Marker =new OpenLayers.Marker(team1LonLat,icon);
	        team1Popup = new OpenLayers.Popup("Equipo1",
	                        team1LonLat,
	                        new OpenLayers.Size(145,52),
	                        "<font size=-2>Equipo 1<br>Lon: "+Math.round(team1Data[1] * 10000) / 10000+", Lat: "+Math.round(team1Data[0] * 10000) / 10000);
	        map.addPopup(team1Popup);
	        team1Popup.hide();
			team1Popup.opacity=0.5;
			team1Marker.events.register('mouseover', team1Marker, function (e) { team1Popup.toggle();OpenLayers.Event.stop (evt); } );
			layerMarkers.addMarker(team1Marker);
						
			goalMarker =new OpenLayers.Marker(goalLonLat,goal);
			layerMarkers.addMarker(goalMarker);
		}

		function updateMarkers(){
			var newPx1 = map.getLayerPxFromViewPortPx(map.getPixelFromLonLat(new OpenLayers.LonLat(team1Data[1], team1Data[0]).transform(map.displayProjection, map.projection)));
			team1Marker.moveTo(newPx1);
			team1Popup.moveTo(newPx1);
			team1Popup.setContentHTML("<font size=-2>Equipo 1<br>Lon: "+Math.round(team1Data[1] * 10000) / 10000+", Lat: "+Math.round(team1Data[0] * 10000) / 10000);
		}
	</script>
</head>
 
<body onLoad="init()">
	<p>hola</p>
	<div id="mapcanvas" style="width: 800px; height: 600px" />
</body>
</html>